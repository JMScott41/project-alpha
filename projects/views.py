from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        create_project_form = ProjectForm(request.POST)
        if create_project_form.is_valid():
            new_category = create_project_form.save(False)
            new_category.owner = request.user
            create_project_form.save()
            return redirect("list_projects")
    else:
        create_project_form = ProjectForm()

    context = {
        "create_project": create_project_form,
    }
    return render(request, "projects/create_project.html", context)
